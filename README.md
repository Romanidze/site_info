# site_info

Сервис для хранения информации о посещениях сайта

# Как запускать(используются PostgreSQL и Redis): 

Для PostgreSQL: 

```
psql -U postgres
CREATE DATABASE reactive_info;
CREATE USER reactive_user WITH PASSWORD 'reactive_pass';
CREATE TABLE measurement(

    id SERIAL PRIMARY KEY,
    user_id INTEGER,
    page_id INTEGER,
    attendance_date VARCHAR(20)

);
```
Для Redis:

Достаточно запустить с помощью redis-server

Запуск самого приложения: 

```
./gradlew bootRun
```

# API

## Создание события посещения сайта пользователем

### URL
localhost:8081/measurements/user_request

### Тип запроса, Content-Type
POST, application/json

### Тело запроса

```
{
    "user_id": 1,
    "page_id": 1
}
```

### Тело ответа

```
{
    "attendance_count": 1,
    "unique_user_count": 1
}
```

## Получение статистики посещения за произвольный период

### URL
localhost:8081/measurements/period_statistics

### Тип запроса, Content-Type
POST, application/json

### Тело запроса

```
{
    "start_date": "3.11.2018",
    "end_date": "4.11.2018"
}
```

### Тело ответа

```
{
    "attendance_count": 1,
    "unique_user_count": 1,
    "stable_user_count": 1
}
```
    
